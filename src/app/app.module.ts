import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {DataService} from './_services/data.service';
import {CookieModule, CookieService} from 'ngx-cookie';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


import {APP_BASE_HREF} from '@angular/common';

import {MainComponent} from './components/main/main';

import {ModalService} from './_services/modal.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatDialogModule} from '@angular/material';

import {HeaderComponent} from './components/header/header.component';

import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {ModalOrderComponent} from './components/modal-order/modal-order.component';
import {OrderService} from './_services/order.service';
import {FooterComponent} from './components/footer/footer.component';
import {LoginComponent} from './components/login/login.component';
import {BalanceComponent} from './components/balance/balance.component';
import {HistoryComponent} from './components/history/history.component';
import {HistoryDetailsComponent} from './components/history-details/history-details.component';
import {ProfileComponent} from './components/profile/profile.component';
import {QuestionnaireComponent} from './components/questionnaire/questionnaire.component';
import {MenuComponent} from './components/menu/menu.component';
import {InformationComponent} from './components/information/information.component';
import {InformationAboutComponent} from './components/information-about/information-about.component';
import {InformationLicenseComponent} from './components/information-license/information-license.component';
import {InformationPrivacyPolicyComponent} from './components/information-privacy-policy/information-privacy-policy.component';
import {FeedbackComponent} from './components/feedback/feedback.component';
import {FeedbackQuestionComponent} from './components/feedback-question/feedback-question.component';
import {FeedbackCommentComponent} from './components/feedback-comment/feedback-comment.component';
import {FeedbackQuestionAppComponent} from './components/feedback-question-app/feedback-question-app.component';
import {HelpComponent} from './components/help/help.component';
import {HelpFilterComponent} from './components/help-filter/help-filter.component';
import {HelpAnswerComponent} from './components/help-answer/help-answer.component';
import {SosComponent} from './components/sos/sos.component';
import {CarComponent} from './components/car/car.component';
import {ConditionComponent} from './components/condition/condition.component';
import {PhotoControlComponent} from './components/photo-control/photo-control.component';
import {TakePhotosComponent} from './components/take-photos/take-photos.component';
import {PhotosGalleryComponent} from './components/photos-gallery/photos-gallery.component';
import {ChatComponent} from './components/chat/chat.component';
import {StatisticsComponent} from './components/statistics/statistics.component';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        HeaderComponent,
        ModalOrderComponent,
        FooterComponent,
        LoginComponent,
        BalanceComponent,
        HistoryComponent,
        HistoryDetailsComponent,
        ProfileComponent,
        MenuComponent,
        InformationComponent,
        InformationAboutComponent,
        InformationLicenseComponent,
        InformationPrivacyPolicyComponent,
        FeedbackComponent,
        FeedbackQuestionComponent,
        FeedbackCommentComponent,
        FeedbackQuestionAppComponent,
        HelpComponent,
        HelpFilterComponent,
        HelpAnswerComponent,
        SosComponent,
        QuestionnaireComponent,
        CarComponent,
        ConditionComponent,
        PhotoControlComponent,
        TakePhotosComponent,
        PhotosGalleryComponent,
        ChatComponent,
        StatisticsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CookieModule.forRoot(),
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production})

    ],
    entryComponents: [],
    providers: [
        OrderService,
        DataService,
        ModalService,
        CookieService,


        {provide: APP_BASE_HREF, useValue: '/'}

    ],
    exports: [MatButtonModule, MatCheckboxModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})
export class AppModule {
}
