import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakePhotosComponent } from './take-photos.component';

describe('MenuComponent', () => {
  let component: TakePhotosComponent;
  let fixture: ComponentFixture<TakePhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakePhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakePhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
