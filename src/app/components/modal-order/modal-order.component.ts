import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-modal-order',
  templateUrl: './modal-order.component.html',
  styleUrls: ['./modal-order.component.scss']
})
export class ModalOrderComponent implements OnInit {

  constructor(public oS:OrderService) { }

  ngOnInit() {
  }
  getAddress(a){
    let s = a.split(',')
    if (s[s.length-2]&&s[s.length-1]){
      return s[s.length-2]+s[s.length-1]
    }
    return a
  }
}
