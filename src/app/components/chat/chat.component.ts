import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {MatDialog} from '@angular/material';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-history',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  orders = []

  constructor(public dataService:DataService,public dialog: MatDialog,private oS:OrderService) {}

  ngOnInit() {
    this.dataService.send('getHistoryDriver', {}).then(data => {
      this.orders = data['orders']



    });

      // document.getElementById("txt").onkeyup = function(){
      //     var getText = this.value;
      //     var getRegs = getText.match(/^.*(\r\n|\n|$)/gim);
      //     var setText = false;
      //     for(var i = 0; i < getRegs.length; i++){
      //         getText = getRegs[i].replace(/\r|\n/g, "");
      //         setText += getText.length ? Math.ceil(getText.length / 50) : 1;
      //     }
      //     this.rows = setText;
      // };
  }

}
