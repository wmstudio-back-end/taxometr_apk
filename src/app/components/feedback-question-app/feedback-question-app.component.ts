import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {MatDialog} from '@angular/material';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-history',
  templateUrl: './feedback-question-app.component.html',
  styleUrls: ['./feedback-question-app.component.scss']
})
export class FeedbackQuestionAppComponent implements OnInit {
  orders = []
  constructor(public dataService:DataService,public dialog: MatDialog,private oS:OrderService) {}

  ngOnInit() {
    this.dataService.send('getHistoryDriver', {}).then(data => {
      this.orders = data['orders']

    })
  }

}
