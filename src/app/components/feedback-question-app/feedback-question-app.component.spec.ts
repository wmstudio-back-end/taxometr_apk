import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackQuestionAppComponent } from './feedback-question-app.component';

describe('MenuComponent', () => {
  let component: FeedbackQuestionAppComponent;
  let fixture: ComponentFixture<FeedbackQuestionAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackQuestionAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackQuestionAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
