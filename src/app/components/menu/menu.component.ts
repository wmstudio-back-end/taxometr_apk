import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {MatDialog} from '@angular/material';
import {OrderService} from '../../_services/order.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  orders = []
  constructor(public dataService:DataService,public dialog: MatDialog,private oS:OrderService, private router: Router) {

  }

  ngOnInit() {

    this.dataService.send('getHistoryDriver', {}).then(data => {
      this.orders = data['orders']

    })
  }

}
