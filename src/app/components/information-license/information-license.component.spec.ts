import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationLicenseComponent } from './information-license.component';

describe('MenuComponent', () => {
  let component: InformationLicenseComponent;
  let fixture: ComponentFixture<InformationLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
