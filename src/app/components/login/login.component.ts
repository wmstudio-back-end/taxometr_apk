import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {AuthService} from '../../_services/auth.service';
import {ModalService} from '../../_services/modal.service';
import {CookieService} from 'ngx-cookie';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  id
  ObjectKeys = Object.keys
  name
  numberAvto
  markAvto
  numberDriver
  typeAvto
  phone
  constructor(public dataService: DataService,
              public authService:AuthService,
              public mS:ModalService,
              private cookieService:CookieService,
              public orderService:OrderService
  ) {


  }
  ngOnInit() {
  }
  login(){

    // this.dataService.send('regDriver',{code:this.id,type:0}).then(data=>{
    //   this.dataService.user = Object.assign(this.dataService.user,data['profile']);
    //   this.dataService.user.auth = true;
    //   this.setSession(data['_id']);
    //   console.log('RSP',data);
    // });
    this.dataService.send('authDriver',{
      name:this.name,
      numberAvto:this.numberAvto,
      markAvto:this.markAvto,
      phone:this.phone,
      numberDriver:this.numberDriver,
      typeAvto:this.typeAvto,
      gps:this.dataService.GPS,
    }).then(data=>{
      this.dataService.user = Object.assign(this.dataService.user,data['profile']);
      this.dataService.user.auth = true;
      localStorage.setItem('token', data['profile']._id);
      localStorage.setItem("user",JSON.stringify(data['profile']));
      console.log('SEND USER LOGIN!!!!',this.dataService.GPS);
      if (this.dataService.GPS.lat&&this.dataService.GPS.lon){
        console.log('SEND GPS!!!!');
        this.dataService.user.gps = this.dataService.GPS
        this.dataService.send('setGPSDriver', {gps: this.dataService.GPS}).then(data => {})
        var online = false
        if (this.dataService.user.online){
          online = true
          this.dataService.send('setStatusOnline',{online:this.dataService.user.online}).then(data=>{

          })
        }

      }
      this.setSession(data['profile']._id);
      console.log('RSP',data);
    });
  }
  setSession(token){
    console.log('PUTTTTT',token);
    localStorage.setItem("token",token);

    this.cookieService.put('tokenDriver', token, {
      expires: new Date(new Date().getTime() *10000000),
      path:    '*'
    });

  }
}
