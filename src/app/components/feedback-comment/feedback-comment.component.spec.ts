import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackCommentComponent } from './feedback-comment.component';

describe('MenuComponent', () => {
  let component: FeedbackCommentComponent;
  let fixture: ComponentFixture<FeedbackCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
