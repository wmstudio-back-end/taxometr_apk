import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationPrivacyPolicyComponent } from './information-privacy-policy.component';

describe('MenuComponent', () => {
  let component: InformationPrivacyPolicyComponent;
  let fixture: ComponentFixture<InformationPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationPrivacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
