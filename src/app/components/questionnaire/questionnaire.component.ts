import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-profile',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {
  city  = true
  constructor(public dS:DataService,public oS:OrderService) { }

  ngOnInit() {

  }
  restartServer(){

    this.dS.send('restartServer',{}).then(data=>{

    })

  }
  createDemo(){
    this.dS.user.online = true
    this.dS.send('setStatusOnline',{online:this.dS.user.online}).then(data=>{

    })
    var tariff = this.city?2:1
    this.dS.send('createInitOrder2',{tariff:tariff}).then(data=>{

    })

  }
}
