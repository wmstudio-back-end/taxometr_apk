import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {MatDialog} from '@angular/material';
import {OrderService} from '../../_services/order.service';

@Component({
  selector: 'app-history',
  templateUrl: './information-about.component.html',
  styleUrls: ['./information-about.component.scss']
})
export class InformationAboutComponent implements OnInit {
  orders = []
  constructor(public dataService:DataService,public dialog: MatDialog,private oS:OrderService) {}

  ngOnInit() {
    this.dataService.send('getHistoryDriver', {}).then(data => {
      this.orders = data['orders']

    })
  }

}
