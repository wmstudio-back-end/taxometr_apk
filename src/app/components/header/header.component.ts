import {Component, OnInit} from '@angular/core';
import {DataService} from '../../_services/data.service';

import {MatDialog} from '@angular/material';
import {CookieService} from 'ngx-cookie';
import {OrderService} from '../../_services/order.service';
import {NavigationEnd, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    audio = new Audio();
    menu = true;
    name = '';
    Order;

    constructor(public dataService: DataService, public dialog: MatDialog, public oS: OrderService, private router: Router, private _location: Location) {
        this.router.events.subscribe((url: any) => {
            if (url instanceof NavigationEnd) {
                if (url.url == '/') {
                    this.name = '';
                    this.menu = true;
                } else if (url.url == '/menu') {
                    this.menu = false;
                    this.name = 'Menu';
                } else if (url.url == '/questionnaire') {
                    this.menu = true;
                    this.name = 'Анкета';
                } else if (url.url == '/profile') {
                    this.menu = false;
                    this.name = 'Водитель';
                } else if (url.url == '/car') {
                    this.menu = false;
                    this.name = 'Автомобиль';
                } else if (url.url == '/condition') {
                    this.menu = false;
                    this.name = 'Условия';
                } else if (url.url == '/help') {
                    this.menu = true;
                    this.name = 'Служба поддержки';
                } else if (url.url == '/help-filter') {
                    this.menu = false;
                    this.name = 'Грузоперевозки';
                } else if (url.url == '/help-answer') {
                    this.menu = false;
                    this.name = 'Ответ';
                } else if (url.url == '/feedback') {
                    this.menu = true;
                    this.name = 'Обратная связь';
                } else if (url.url == '/feedback-question') {
                    this.menu = false;
                    this.name = 'Задать вопрос';
                } else if (url.url == '/feedback-comment') {
                    this.menu = false;
                    this.name = 'Отзыв или предложение';
                } else if (url.url == '/feedback-question-app') {
                    this.menu = false;
                    this.name = 'Вопрос по приложению';
                } else if (url.url == '/information') {
                    this.menu = true;
                    this.name = 'Информация';
                } else if (url.url == '/information-about') {
                    this.menu = false;
                    this.name = 'О приложении';
                } else if (url.url == '/information-license') {
                    this.menu = false;
                    this.name = 'Лицензионное соглашение';
                } else if (url.url == '/information-privacy-policy') {
                    this.menu = false;
                    this.name = 'Политика конфиденциальности';
                } else if (url.url == '/photo-control') {
                    this.menu = true;
                    this.name = 'Фотоконтроль';
                } else if (url.url == '/take-photos') {
                    this.menu = true;
                    this.name = 'Вид ...';
                } else if (url.url == '/photos-gallery') {
                    this.menu = true;
                    this.name = 'Фотографии';
                } else if (url.url == '/history') {
                    this.menu = true;
                    this.name = 'История';
                } else if (url.url == '/history-details') {
                    this.menu = false;
                    this.name = '#145232';
                } else if (url.url == '/chat') {
                    this.menu = true;
                    this.name = 'Сообщения';
                }
                else if (url.url == '/statistics') {
                    this.menu = true;
                    this.name = 'Статистика';
                }


                console.log('======>', url.url);

            }

        });
        this.audio.src = 'assets/blink.mp3';
        this.audio.load();
    }

    cancel() {
        this._location.back();
    }

    color = 'accent';
    checked = false;
    disabled = false;
    GPSARRAY = '[56.83435616232148,53.228762207859006]';
    ACCR = 50;

    // dataService.GPS.lat
    ngOnInit() {

    }

    setStatusOnline() {
        this.dataService.user.online = !this.dataService.user.online;
        this.dataService.send('setStatusOnline', {online: this.dataService.user.online}).then(data => {

        });
    }

}
