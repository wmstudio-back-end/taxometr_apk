import { Component, OnInit } from '@angular/core';
import {CookieService}    from 'ngx-cookie';
import {DataService} from '../../_services/data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {MatDialogModule} from '@angular/material/dialog';
import {OrderService} from '../../_services/order.service';
declare var ymaps;
declare var $;
@Component({
  templateUrl: './main.html',
  styleUrls: ['./main.scss']
})
export class MainComponent implements OnInit {
  id;
  audio = new Audio();
  Order;
  animal: string;
  name: string;
  GPSARRAY = '[56.83435616232148,53.228762207859006]'
  ACCR = 50
  map_viwe = ()=>{if (this.oS.Order){return false}return true;}
  constructor(public dataService:DataService,private cookieService:CookieService,public dialog: MatDialog,public oS:OrderService) {
    this.audio.src = 'assets/blink.mp3';
    this.audio.load();
  }

  ngOnInit() {
   setTimeout(this.initMap.bind(this),2000)
  }
  initMap(){
    try {
      if (true){

        this.dataService.myMap = new ymaps.Map('mapYa', {
          center: [56.83527490, 53.22971965],
          zoom: 16,
          // controls: ['smallZoomControl']
        }, {
          searchControlProvider: 'yandex#search'
        })
        this.dataService.myMap.controls.remove("mapTools")
          .remove("typeSelector")
          .remove("searchControl")
          .remove("trafficControl")
          .remove("miniMap")
          .remove("scaleLine")
          .remove("routeEditor")
          .remove("smallZoomControl");
      }

    }catch (e) {

    }
  }
  sendGPS(){
    var t = this.GPSARRAY.split(",")
    var lat = parseFloat(t[0].slice(1))
    var lon = parseFloat(t[1].substring(0, t[1].length - 1))
    this.oS.onSuccess({coords:{
        latitude:lat,
        longitude:lon,
        accuracy:this.ACCR,
      }

    })
    // this.dataService.GPS.lat = lat
    // this.dataService.GPS.lon = lon
    this.dataService.send('setGPSDriver',{gps:this.dataService.GPS}).then(data=>{

    })
  }
  getAddress(a){
    let s = a.split(',')
    if (s[s.length-2]&&s[s.length-1]){
      return s[s.length-2]+s[s.length-1]
    }
    return a
  }
  setStatusOnline(val){
    this.dataService.user.online = val
    this.dataService.send('setStatusOnline',{online:this.dataService.user.online}).then(data=>{

    })
  }
  secToString(sec){

    var h = sec/3600 ^ 0 ;
    var m = (sec-h*3600)/60 ^ 0 ;
    var s = sec-h*3600-m*60 ;
    var hh = h>0?(h<10?"0"+h:h)+" ч. ":''
    var mm = m>0?(m<10?"0"+m:m)+" мин. ":''
    var ss = (s<10?parseInt("0"+s):parseInt(s.toString()))+" сек."
    return  hh+mm+ss;

  }
  wayToKm(way){
    return way.toFixed(3)

  }

}
