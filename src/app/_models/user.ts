export interface User {
    token:string
    avatar: string
    email: string
    info: string
    phone: string
    sex: string
    username: string
    web: string
}

export interface UserEdit {
    email: string
    info: string
    phone: string
    sex: string
    username: string
    web: string
}
