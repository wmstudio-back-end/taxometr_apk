import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MainComponent} from './components/main/main';
import {BalanceComponent} from './components/balance/balance.component';
import {HistoryComponent} from './components/history/history.component';
import {HistoryDetailsComponent} from './components/history-details/history-details.component';
import {ProfileComponent} from './components/profile/profile.component';
import {QuestionnaireComponent} from './components/questionnaire/questionnaire.component';
import {MenuComponent} from './components/menu/menu.component';
import {InformationComponent} from './components/information/information.component';
import {InformationAboutComponent} from './components/information-about/information-about.component';
import {InformationLicenseComponent} from './components/information-license/information-license.component';
import {InformationPrivacyPolicyComponent} from './components/information-privacy-policy/information-privacy-policy.component';
import {FeedbackComponent} from './components/feedback/feedback.component';
import {FeedbackQuestionComponent} from './components/feedback-question/feedback-question.component';
import {FeedbackCommentComponent} from './components/feedback-comment/feedback-comment.component';
import {FeedbackQuestionAppComponent} from './components/feedback-question-app/feedback-question-app.component';
import {HelpComponent} from './components/help/help.component';
import {HelpFilterComponent} from './components/help-filter/help-filter.component';
import {HelpAnswerComponent} from './components/help-answer/help-answer.component';
import {SosComponent} from './components/sos/sos.component';
import {CarComponent} from './components/car/car.component';
import {ConditionComponent} from './components/condition/condition.component';
import {PhotoControlComponent} from './components/photo-control/photo-control.component';
import {TakePhotosComponent} from './components/take-photos/take-photos.component';
import {PhotosGalleryComponent} from './components/photos-gallery/photos-gallery.component';
import {ChatComponent} from './components/chat/chat.component';
import {StatisticsComponent} from './components/statistics/statistics.component';

const routes: Routes = [
    {path:'', component:MainComponent, pathMatch: 'full'},
    // {path:'/', component:MainComponent},
    {path:'balance', component:BalanceComponent},
    {path:'history', component:HistoryComponent},
    {path:'history-details', component:HistoryDetailsComponent},
    {path:'profile', component:ProfileComponent},
    {path:'questionnaire', component:QuestionnaireComponent},
    {path:'menu', component:MenuComponent},
    {path:'information', component:InformationComponent},
    {path:'information-about', component:InformationAboutComponent},
    {path:'information-license', component:InformationLicenseComponent},
    {path:'information-privacy-policy', component:InformationPrivacyPolicyComponent},
    {path:'feedback', component:FeedbackComponent},
    {path:'feedback-question', component:FeedbackQuestionComponent},
    {path:'feedback-comment', component:FeedbackCommentComponent},
    {path:'feedback-question-app', component:FeedbackQuestionAppComponent},
    {path:'help', component:HelpComponent},
    {path:'help-filter', component:HelpFilterComponent},
    {path:'help-answer', component:HelpAnswerComponent},
    {path:'sos', component:SosComponent},
    {path:'car', component:CarComponent},
    {path:'condition', component:ConditionComponent},
    {path:'photo-control', component:PhotoControlComponent},
    {path:'take-photos', component:TakePhotosComponent},
    {path:'photos-gallery', component:PhotosGalleryComponent},
    {path:'chat', component:ChatComponent},
    {path:'statistics', component:StatisticsComponent},
    {path:'**', component:MainComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
