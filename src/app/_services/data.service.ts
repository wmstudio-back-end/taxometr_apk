import { Injectable, EventEmitter } from '@angular/core';
import {CookieService}    from 'ngx-cookie';
import { HttpClient,HttpHeaders,HttpRequest } from '@angular/common/http';
import {AuthService} from './auth.service';
import {ModalService} from './modal.service';
import {OrderService} from './order.service';
import KalmanFilter from 'kalmanjs';
declare var ymaps;
declare var io;
declare var window;
// declare var KalmanFilter;

export interface Message {
  method: string,
  data: any,
  requestId: number,

}

@Injectable({
    providedIn: 'root'
})

export class DataService {
  modal = false
  ready = false
  GPS = {
    lat:null,
    lon:null,
    last_lat:null,
    last_lon:null,
    lastGPS:function(){
      let lat = this.last_lat
      let lon = this.last_lon
      this.last_lat = this.lat
      this.last_lon = this.lon
      return  {lat:lat,lon:lon}
    },
  }
  user = {
    auth : false,
    online:false,
    name:'',
    gps:null
  }
  wsRecconect: any;
  ws: any;
  requestId: number = 0;
  private message = <Message>{};
  public req = {}
  watch =null;
  reconnect
  public messages = {
    "EVENT/createInitOrder": new EventEmitter(),
    "EVENT/cancelOrder": new EventEmitter(),
    "EVENT/options": new EventEmitter(),

  }
  OPTIONS = {
    typeAvto : {
      "1":{
        name:"каблук",
        maxWeight:600,
        maxLength:200,
        firstHour:500,
        nextHour:400,
        kilometer:20,
        kilometerback:20,
        supply:200

      },
      "2":{
        name:"Газель 3м",
        maxWeight:1500,
        maxLength:3000,
        firstHour:700,
        nextHour:600,
        kilometer:36,
        kilometerback:18,
        supply:200
      },
      "3":{
        name:"Газель 4м",
        maxWeight:1500,
        maxLength:4000,
        firstHour:800,
        nextHour:600,
        kilometer:36,
        kilometerback:18,
        supply:200
      },
      "4":{
        name:"Газель 6м",
        maxWeight:1500,
        maxLength:6000,
        firstHour:900,
        nextHour:600,
        kilometer:36,
        kilometerback:18,
        supply:200
      },
      "5":{
        name:"Газ 5 тонн",
        maxWeight:3000,
        maxLength:5000,
        firstHour:1100,
        nextHour:800,
        kilometer:40,
        kilometerback:20,
        supply:200
      }
    }
  }
  myMap
  myPlacemarkWithContent



  constructor(private http:HttpClient,private mS:ModalService,private cookieService: CookieService) {

    this.connectWs()
    this.messages['EVENT/options'].subscribe(data=>{
      this.OPTIONS = data
    })
    this.GPS.lat
    this.GPS.lon
    var user = localStorage.getItem("user");
    if (user){
      this.user = JSON.parse(user)
    }


    // setInterval(function(){
    //   console.log();
    //   this.priceM = parseFloat((this.startM*36+this.stayM).toFixed(2))
    // }.bind(this),1000)
  }
  anmatCar(start,end){

    var lat = start[0]
    var lon = start[1]
    let lat_end = false
    let lon_end = false
    // coords = firstGeoObject.geometry.getCoordinates(),
    console.log('CCOOORRRDS',start);
    var mod1 = 0
    var mod2 = 0
    var icon = 'U'


    this.GPS.lat>lat?mod1 =this.GPS.lat-lat:mod1 = lat-this.GPS.lat
    this.GPS.lon>lon?mod2 =this.GPS.lon-lon:mod2 = lon-this.GPS.lon

    var modul = Math.max.apply(null, [mod1,mod2])

    mod1>mod2?mod1>0?icon = 'D':icon = 'U':mod2>0?icon = 'L':icon = 'R'
    if (mod1>mod2){
      if (this.GPS.lat>lat){icon = 'U'}
      if (this.GPS.lat<lat){icon = 'D'}
    }
    if (mod2>mod1){
      if (this.GPS.lon>lon){icon = 'R'}
      if (this.GPS.lon<lon){icon = 'L'}
    }



    // console.log('MOD-1',mod1);
    // console.log('MOD-2',mod2);
    // console.log('modul',modul);
    // console.log('кадров ',modul/0.00001);
    // console.log('rflhjd ',5000/(modul/0.00001));

    function isStop(){
      if (this.GPS.lat>start[0]&&this.GPS.lon>start[1]){

      }


      if (this.GPS.lat>start[0]){if (lat>this.GPS.lat){lat_end=true;lat = this.GPS.lat}}else if (this.GPS.lat<start[0]){if (lat<this.GPS.lat){lat_end=true;lat = this.GPS.lat}}
      if (this.GPS.lon>start[1]){if (lon>this.GPS.lon){lon_end=true;lon = this.GPS.lon}}else if (this.GPS.lon<start[1]){if (lon<this.GPS.lon){lon_end=true;lon = this.GPS.lon}}
     if (lat_end&&lon_end){
       clearInterval(timeout)
       lat = this.GPS.lat
       lon = this.GPS.lon
     }

      // if (this.GPS.lat>coords[0]&&lat>this.GPS.lat){
      //   lat = this.GPS.lat
      //   clearInterval(timeout)
      // }
      // if (this.GPS.lat<coords[0]&&lat<this.GPS.lat){
      //   lat = this.GPS.lat
      //   clearInterval(timeout)
      // }
      // if (this.GPS.lon>coords[1]&&lon>this.GPS.lon){
      //   lon = this.GPS.lon
      //   clearInterval(timeout)
      // }
      // if (this.GPS.lon<coords[1]&&lon<this.GPS.lon){
      //   lon = this.GPS.lon
      //   clearInterval(timeout)
      // }
    }

    console.log('DRIVE!!!',icon);
    var timeout = setInterval(function(){
      if (this.GPS.lat>lat&&!lat_end){lat+=0.0001;}else if (this.GPS.lat<lat){lat-=0.0001;}
      if (this.GPS.lon>lon&&!lon_end){lon+=0.0001;}else if (this.GPS.lon<lon){lon-=0.0001;}

      isStop.bind(this)()
      this.myPlacemarkWithContent.geometry.setCoordinates([lat, lon]);
    }.bind(this),20)
    this.MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
      '<div class="avto_baloon" style="color: green; font-weight: bold;text-align: center">$[properties.iconContent]</div>'
    )
    var result = ymaps.geoQuery(this.myPlacemarkWithContent);
    result.setOptions({
      // Опции.
      // Необходимо указать данный тип макетthis.myMap.geoObjects.remove(а.
      iconLayout: 'default#imageWithContent',
      // Своё изображение иконки метки.
      iconImageHref: 'assets/avto_icon_'+icon+'.png',
      // Размеры метки.
      iconImageSize: [48, 48],
      // Смещение левого верхнего угла иконки относительно
      // её "ножки" (точки привязки).
      iconImageOffset: [-24, -24],
      // Смещение слоя с содержимым относительно слоя с картинкой.
      iconContentOffset: [-5, 35],
      // Макет содержимого.
      iconContentLayout: this.MyIconContentLayout
    })


  }
 MyIconContentLayout

  addToMap(){
    // this.myMap.objectManager.removeAll();
    if (!this.myMap){return}
    if (!this.myPlacemarkWithContent){
      this.MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div class="avto_baloon" style="color: red; font-weight: bold;text-align: center">$[properties.iconContent]</div>'
      )
      this.myPlacemarkWithContent = new ymaps.Placemark([this.GPS.lat, this.GPS.lon], {
        hintContent: 'Собственный значок метки с контентом',
        balloonContent: this.user.name,
        iconContent: this.user.name
      }, {
        // Опции.
        // Необходимо указать данный тип макетthis.myMap.geoObjects.remove(а.
        iconLayout: 'default#imageWithContent',
        // Своё изображение иконки метки.
        iconImageHref: 'assets/avto_icon_U.png',
        // Размеры метки.
        iconImageSize: [48, 48],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-24, -24],
        // Смещение слоя с содержимым относительно слоя с картинкой.
        iconContentOffset: [-5, 35],
        // Макет содержимого.
        iconContentLayout: this.MyIconContentLayout
      });
      this.myMap.geoObjects
        .add(this.myPlacemarkWithContent)
    }else{
      var coords = this.myPlacemarkWithContent.geometry.getCoordinates()
      // this.myPlacemarkWithContent.geometry.setCoordinates([this.GPS.lat, this.GPS.lon]);
      this.anmatCar(coords,[this.GPS.lat, this.GPS.lon])

    }
    // this.myMap.geoObjects.remove(this.myPlacemarkWithContent);

  }




  send(method: string, data: any, server?: string) {
    if (!server) {
      server = 'worker';
    }

    this.requestId++;
    this.message = <Message>{
      method: method,
      data: data,
      error: null,
      requestId: this.requestId
    };
    this.req[this.requestId] = new EventEmitter();
    try {
      this.ws.send(JSON.stringify(this.message));  
    }catch (e) {
      
    }
    
    console.log('SEND TO SERVER ', this.message);
    return new Promise((resolve, reject) => {
      this.req[this.requestId].subscribe((data) => {
        return resolve(data);
      });
    });
  }


  connectWs() {
    console.log('____________',this.cookieService.get('tokenDriver'));

    // this.ws = new WebSocket("ws://192.168.102.231:7001");
    this.ws = new WebSocket("ws://needcar18.ru:7001");
    this.ws.onclose = (event)=> {
      if (event.wasClean) {
        console.log('Соединение закрыто чисто');
      } else {
        console.log('Обрыв соединения'); // например, "убит" процесс сервера
      }
      console.log('Код: ' + event.code + ' причина: ' + event.reason);
      this.ready = false
      setTimeout(function() {
        this.connectWs();
      }.bind(this), 1000);
      console.log('disconnect')
    };

    this.ws.onmessage = function(res){

      this.cookieService.put('token', this.cookieService.get('tokenDriver'), {
        expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
        path: '/'
      });
      var data = JSON.parse(res.data)
      if (data.method=='authDriver'&&!data.data){

        localStorage.removeItem("token");
        localStorage.removeItem("user");
        localStorage.removeItem("Order");
        this.user.auth = false
      }
      console.log('RES SERVER ', data);
      if (!data.data||data.error) {
        if (data.error&&data.error.code) {

          // this.popS.show_info(this.getOptionsFromId('errors', data.error.code).text);
        }
        else if (data.error&&data.error.errorId) {
          // this.popS.show_info(this.getOptionsFromId('errors', data.error.errorId).text);
        }
        else {
          console.log('NODATA');
          // this.popS.show_info(data.error.text || data.error.error.text);
        }

        if (!data.data) {

        }
      }

      if (this.req[data.requestId]) {

        this.req[data.requestId].emit(data.data);
      } else {

        if (this.messages[data.method]){
          this.messages[data.method].emit(data.data);
        }else{
          console.log('INCOMING MESS',data);
        }

      }


    }.bind(this);

    this.ws.onerror = (error) =>{
      console.log("Ошибка " , error);
    };
    this.ws.onopen = function (data) {
      console.log('CONNECT',this.ready)
      this.ready = true
      // localStorage.setItem("bgColor","green");
      var token = localStorage.getItem("token");
      // this.cookieService.get('token')

      if (token){

        this.send('authDriver',{token:this.cookieService.get('tokenDriver')}).then(data=>{
          var online = false

          if (this.GPS.lat&&this.GPS.lon){
            this.user.gps = this.GPS
            this.send('setGPSDriver', {gps: this.GPS}).then(data => {})
            if (this.user.online){
              online = true
              this.send('setStatusOnline',{online:this.user.online}).then(data=>{

              })
            }

          }
          if (data&&data['profile']){
            this.user = Object.assign(this.user,data['profile'])
            this.user.auth = true
            this.user.online = online
            localStorage.setItem("user",JSON.stringify(this.user));
            console.log('RSP',data)
          }


        })
      }else{


      }

    }.bind(this)


  }





  closeWS() {
  }




}
