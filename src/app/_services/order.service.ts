import { Injectable } from '@angular/core';
import {DataService} from './data.service';
import KalmanFilter from 'kalmanjs';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  Order = null
  newOrderModal = false
  timerTrackOrder = null

  audio = new Audio();
  TAXOMETR
  kf_lat = new KalmanFilter();
  kf_lon = new KalmanFilter();
  LOGS
  timeOutFromStayAvto = null
  step_error = 800
  step_way = 30
  priceM = 0
  stayAvto = false
  timerStayAvto
  startM = 0
  stayM = 0

  taxometr = function(self){
    this.self = self
    this.way = 0//пройденный километраж
    this.timeStay = 0//время простоя машины
    this.time = 0//время с момента взятия* заказа
    this.price = 0//стоимость

    this.TIME_DRIVE_timer
    this.TIME_SENDSTATUS_timer

    if (self.Order&&self.Order.hasOwnProperty('taxometr')){

      let t1 = + new Date(self.Order.startWorkOrder)/1000;
      let t2 = + new Date()/1000;
      console.log(t2-t1);
      this.way = self.Order.taxometr.way
      this.timeStay = self.Order.taxometr.timeStay
      this.price = self.Order.taxometr.price
      this.time =  t2-t1
      this.self.Order.taxometr.time = this.time

    }else{
      this.self.Order.taxometr = {
        way:this.way,
        timeStay:this.timeStay,
        price:this.price,
        time:0
      }
    }


    this.start = function(){
      this.TIME_DRIVE_timer = setInterval(()=>{
        this.time++;

        var price = 0
        if (this.self.Order.tariff==1){
          var summHour = this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour  //firstHour //nextHour
          if (this.time>3600){
            summHour = this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].nextHour
          }

          price = parseFloat((this.time*summHour/3600).toFixed(2))||0

        }
        if (this.self.Order.tariff==2){
          price = parseFloat((this.timeStay*this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].nextHour/3600+this.way*this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].kilometer).toFixed(2))||0
          price+=200
        }
        this.self.Order.taxometr = {
          way:this.way,
          timeStay:this.timeStay,
          price:parseFloat((price).toFixed(2)),
          time:this.time
        }
        },1000)
      this.TIME_SENDSTATUS_timer = setInterval(this.setStatusOrder.bind(this),1000)
    }
    this.end = function(){

      if (this.self.Order.tariff==1){
        if (this.time<3600){
          this.self.Order.taxometr.price = this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour
        }
      }
      if (this.self.Order.tariff==2){
        if (this.self.Order.taxometr.price<this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour){
          this.self.Order.taxometr.price = this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour
        }
      }
      if (this.self.Order.tariff==3){
        if (this.self.Order.taxometr.price<this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour){
          this.self.Order.taxometr.price = this.self.dataService.OPTIONS.typeAvto[this.self.Order.type].firstHour
        }
      }
      this.setStatusOrder()

      clearInterval(this.TIME_DRIVE_timer)
      clearInterval(this.TIME_SENDSTATUS_timer)
      this.self.TAXOMETR = null
      localStorage.setItem("Order",JSON.stringify(this.self.Order));
    }

    this.checkWay = function(way){
      this.way+=way

    }
    this.checkStay = function(stay){

      this.timeStay+=1

    }

    this.setStatusOrder = function(){
     // console.log('STATUS ORDER!!!',this.self.dataService.GPS);
     // console.log(this.self.dataService.GPS.lastGPS());
      // this.self.dataService.send('sendStatusOrder', {taxometr: this.self.Order.taxometr}).then(data => {})
      localStorage.setItem("Order",JSON.stringify(this.self.Order));
     // var token = localStorage.getItem("token");
     //  console.log("this.way",this.way,this.timeStay);

    }




    // timer:function(){}
  }

  constructor(private dataService:DataService) {
    this.audio.src = 'assets/blink.mp3';
    this.audio.load();
    this.dataService.messages['EVENT/createInitOrder'].subscribe(data=>{
      // this.openDialog(data['order']);
      this.newOrderModal = true


      if (this.Order){
        // this.startTaxometr()
      }else if(!this.Order&&data['order'].status==1){
        this.Order = data['order']

    }else if(!this.Order&&data['order'].status==3){
        this.Order = data['order']
        this.startTaxometr()
      }else{
        this.Order = data['order']
      }

      // this.startTimerOrder()
      console.log(this.Order);
      this.audio.play();
    })
    this.dataService.messages['EVENT/cancelOrder'].subscribe(data=>{
      // this.openDialog(data['order']);
      this.newOrderModal = false
      this.Order = null
      // this.startTimerOrder()
      console.log(this.Order);
    })
    setTimeout(this.getLatLon.bind(this),1000)
    var Order = localStorage.getItem("Order");
    if (Order&&!this.Order ){
      var t = JSON.parse(Order)
      if (t&&t.status<4){
        this.Order = JSON.parse(Order)
        this.startTaxometr()
      }

    }
  }
   getRandomArbitary(min, max)
  {
    return Math.random() * (max - min) + min;
  }
  getLatLon()  {
    var options = {
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0
    };

    var arrGps = [{lat:56.83527490,lon:53.22971965}]
    var lat = 56.83527490
    var lon = 53.22971965
    for (var i = 0; i < 50; i++) {
      lon+=0.001
      arrGps.push({lat:lat,lon:lon})

    }
    var t = 0
    setInterval(function(){
      var position = {
        coords:{
          latitude:lat+=this.getRandomArbitary(-0.001,0.001),
          longitude:lon+=this.getRandomArbitary(-0.007,0.009),
          accuracy:5
        }
      }
      // console.log(t++);
      // this.onSuccess(position)
    }.bind(this),3000)


    console.log(arrGps);
    navigator.geolocation.getCurrentPosition(this.onSuccess.bind(this), this.onError.bind(this),options);
    navigator.geolocation.watchPosition(this.onSuccess.bind(this), this.onError.bind(this),options);
    //setTimeout(this.getLatLon.bind(this),1000)

  }

  onSuccess(position) {

    this.dataService.GPS.lat = position.coords.latitude//this.kf_lat.filter(position.coords.latitude)
    this.dataService.GPS.lon = position.coords.longitude//this.kf_lon.filter(position.coords.longitude)
    if (!this.dataService.GPS.last_lat||!this.dataService.GPS.last_lon){
      this.dataService.GPS.last_lat = position.coords.latitude//this.kf_lat.filter(position.coords.latitude)
      this.dataService.GPS.last_lon = position.coords.longitude//this.kf_lon.filter(position.coords.longitude)
    }
    var way = 0

    // var lat = this.kf_lat.filter(position.coords.latitude)
    // var lon = this.kf_lon.filter(position.coords.longitude)
    var lat = position.coords.latitude
    var lon = position.coords.longitude


    // this.dataService.myMap ? this.dataService.myMap.setCenter([this.dataService.GPS.lat,this.dataService.GPS.lon], 16) : null
    this.dataService.myMap ? this.dataService.myMap.panTo([this.dataService.GPS.lat,this.dataService.GPS.lon], {flying: true}) : null
    this.dataService.addToMap()
    way = this.getDistanceFromLatLonInKm(this.dataService.GPS.last_lat, this.dataService.GPS.last_lon, this.dataService.GPS.lat, this.dataService.GPS.lon) * 1000||0
    console.log("РАССТОЯНИЕ = ",way);
    if (!this.timeOutFromStayAvto){
      console.log("отсчет до следующих координат");
      this.timeOutFromStayAvto = setTimeout(function(){
        console.log('считаем что машина остановилась');
        this.stayAvto = true
        this.timerStayAvto = setInterval(this.updateTimerStay.bind(this),1000*1)
      }.bind(this),1000*10)
    }



    // this.dataService.send('setGPSDriver', {gps: this.dataService.GPS}).then(data => {})


    if ( way > this.step_way) {
      this.dataService.send('setGPSDriver', {gps: this.dataService.GPS}).then(data => {})
      console.log('========================================================================');
          this.stayAvto = false
      clearInterval(this.timerStayAvto)
      clearTimeout(this.timeOutFromStayAvto)
      this.timeOutFromStayAvto = null
      this.dataService.GPS.last_lat = position.coords.latitude//this.kf_lat.filter(position.coords.latitude)
      this.dataService.GPS.last_lon = position.coords.longitude//this.kf_lon.filter(position.coords.longitude)
          if (this.Order&&this.Order.status==3){
            this.TAXOMETR.checkWay(way/1000)
          }
        }

    // this.LOGS = {
    //   latitude:this.kf_lat.filter(position.coords.latitude),
    //   longitude:this.kf_lon.filter(position.coords.longitude),
    //   accuracy:position.coords.accuracy,
    // }


    // if (!this.timeOutFromStayAvto){
    //
    //   this.timeOutFromStayAvto = setTimeout(function(){
    //     console.log('START TIMEOUT');
    //     this.timerStayAvto = setInterval(this.updateTimerStay.bind(this),1000*1)
    //   }.bind(this),1000*10)
    // }
    // if ( this.LOGS&& this.LOGS.accuracy<this.step_error) {
    // if ( this.LOGS) {
    //
    //   var way = this.getDistanceFromLatLonInKm(this.LOGS.latitude, this.LOGS.longitude, this.dataService.GPS.lat, this.dataService.GPS.lon) * 1000
    //   this.dataService.addToMap()
    //   if ( way > this.step_way) {
    //     this.stayAvto = false
    //     clearInterval(this.timerStayAvto)
    //     clearTimeout(this.timeOutFromStayAvto)
    //     this.timeOutFromStayAvto = null
    //
    //     if (this.Order&&this.Order.status==3){
    //       this.TAXOMETR.checkWay(way/1000)
    //     }
    //   }
    //   if (true) {
    //
    //
    //     // this.startM += way/1000
    //     this.dataService.GPS.lat = this.LOGS.latitude
    //     this.dataService.GPS.lon = this.LOGS.longitude
    //     // this.dataService.send('setGPSDriver', {gps: this.dataService.GPS}).then(data => {})
    //     this.dataService.myMap ? this.dataService.myMap.setCenter([this.dataService.GPS.lat,this.dataService.GPS.lon], 16) : null
    //   }
    // }
  };
  updateTimerStay(){
    if (this.Order&&this.Order.status==3){
      this.TAXOMETR.checkStay()
      this.stayM +=parseFloat((600/3600).toFixed(2))
    }

  }
  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    let a =
      Math.sin((lat2-lat1)* (Math.PI/180)/2) * Math.sin((lat2-lat1)* (Math.PI/180)/2) +
      Math.cos((lat1)* (Math.PI/180)) * Math.cos((lat2)* (Math.PI/180)) *
      Math.sin((lon2-lon1)* (Math.PI/180)/2) * Math.sin((lon2-lon1)* (Math.PI/180)/2)
    return 6371 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  }
  onError(error) {
    // alert('code: '    + error.code    + '\n' +
    //   'message: ' + error.message + '\n');
  }



  finishOrderDrive(){
    this.Order.status = 4
    this.TAXOMETR.end()
    this.dataService.send('finishOrderDrive',{taxometr:this.Order.taxometr}).then(data=>{
      // this.Order = null

    })
  }
  calculateOrder(){
    var price = 0


    switch (this.Order.tariff) {
      case 1:

        if (this.Order.time<3600){this.Order.time = 0;}
        else{
          this.Order.time -=3600
        }
        console.log('меньше часа',this.Order.time)
        var o = (this.Order.time/3600)
        console.log('msg.time-3600/3600',o)
        price = this.dataService.OPTIONS.typeAvto[this.Order.type].firstHour
        console.log('price',price)
        price += o*this.dataService.OPTIONS.typeAvto[this.Order.type].nextHour
        break
      case 2:
        console.log("Длинна ",(this.Order.length/1000));
        price = this.dataService.OPTIONS.typeAvto[this.Order.type].supply
        price += (this.Order.length/1000)*this.dataService.OPTIONS.typeAvto[this.Order.type].kilometer
        break;
      case 3:
        price = this.dataService.OPTIONS.typeAvto[this.Order.type].supply
        price += (this.Order.length/1000)*this.dataService.OPTIONS.typeAvto[this.Order.type].kilometerback
        break;
    }
    console.log('PRICE ',price);
  }
  startTimerOrder(){
    this.timerTrackOrder = setInterval(this.calculateOrder.bind(this),1000)
  }
  clearOrder(){
    this.Order = null

    localStorage.removeItem("Order");
  }
  readyTostartDrive(){
    this.dataService.send('readyTostartDrive',{}).then(data=>{
      this.Order = data
       this.startTaxometr()
      // this.newOrderModal = false
      console.log("readyTostartDrive",data);
      // this.startTimerOrder()
    })
  }
  cencelOrder(){
    this.dataService.send('cancelOrder',{}).then(data=>{
      this.Order = null
      this.newOrderModal = false
      console.log("cancelOrder",data);
    })

  }
  applyOrder(){
    this.dataService.send('applyOrder',{}).then(data=>{
      // this.Order = null
      this.Order = data['order']

      this.newOrderModal = false
      console.log("applyOrder",data);
    })
  }
  startTaxometr(){
    this.TAXOMETR =  new this.taxometr(this)
    console.log('INIT TAXOMETR');
    this.TAXOMETR.start()


  }
}
