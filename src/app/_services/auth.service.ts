import { Injectable } from '@angular/core';

import {CookieService} from "ngx-cookie";
import {DataService} from "./data.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public auth = false
  public loading = true

  constructor(private cookieService: CookieService,private dataService:DataService, public router: Router) {

  }

}
