import {Component, OnInit} from '@angular/core';
import {DataService} from "./_services/data.service";
import {AuthService} from "./_services/auth.service";
import {ModalService} from "./_services/modal.service";
import {CookieService} from 'ngx-cookie';
import {OrderService} from './_services/order.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
    title = 'taxi';
    isLogin = false
    spiner = false
    Flogin = true
    Freg = false
    Fremind = false
    email = 'severomorets@mail.ru'
    username = '123456'
    pass = '123456'

    cutDescription(description) {
        if (description.length > 100) {
            return description.slice(0, 100)
        }
        return description;
    }

    constructor(public dataService: DataService,public authService:AuthService,public mS:ModalService,private cookieService:CookieService,public oS:OrderService) {


    }
    ngOnInit() {}



}
