



$(document).ready(function () {

    $('.hash-input').hashTagInput({
        showHashTags: true,
        hideInput: true,
        formatAsBubbles: true,
        wrapTags: true
    });
    $('.tabs__caption').on('click', '.pasiv:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.footer_weble').on('click', '.click_img:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest("body").find('.tabs__footer').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.choice_pers_content').on('click', '.my_content:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.personal_content').find('.tabs_pers').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.creation_img_caption').on('click', '.creation_img:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.create_photo_modal').find('.ways_creation_img').removeClass('active').eq($(this).index()).addClass('active');
    });


    $('.people_tags_tab').on('click', '.people_tags:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.tabs_search').find('.block_search').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('.hashtage_caption').on('click', '.hashtage_tab:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.hashtage_post').find('.hashtage_content').removeClass('active').eq($(this).index()).addClass('active');
    });


    $('.three_points').click(function () {
        $('.three_points_modal').toggleClass('dis_block');
    });
    $('.settings_view, .gray_bar_right').click(function () {
        $('.types_posts_choice').toggleClass('dis_block');
        $('body').toggleClass('hidden_body');
    });

    $('.view_modal_shadow').click(function () {
        $('.types_posts_choice').removeClass('dis_block');
        $('body').removeClass('hidden_body');
    });
    $('.view_classic').click(function () {
        $('.gray_bar_right .two_rectangles, .view_classic .tick_type_png').removeClass('dis_none dis_none_text');
        $('.gray_bar_right .miniatures_tabs, .view_miniatures .tick_type_png').removeClass('dis_block');
        $('.gray_bar_right .text_tabs, .view_text .tick_type_png').removeClass('dis_block');
        $('.post_one .post_mid').removeClass('miniatures_post_one miniatures_style');
        $('.post_two .post_mid').removeClass('miniatures_post_two miniatures_style');
        $('.post_icon, .post_top_text, .point_post, .post_class, .post_time').removeClass('minpost');
        $('.post_three.white_bac').removeClass('dis_none_text');
        $('.post_three.white_bac').removeClass('dis_none');
        $('.post_middle').removeClass('border_none');
        $('.post_middle_img').removeClass('dis_none');
    });
    $('.view_miniatures').click(function () {
        $('.gray_bar_right .miniatures_tabs, .view_miniatures .tick_type_png').removeClass('dis_none');
        $('.gray_bar_right .two_rectangles, .view_classic .tick_type_png').removeClass('dis_none_text');
        $('.gray_bar_right .two_rectangles, .view_classic .tick_type_png').addClass('dis_none');
        $('.gray_bar_right .miniatures_tabs, .view_miniatures .tick_type_png').addClass('dis_block');
        $('.gray_bar_right .text_tabs, .view_text .tick_type_png').removeClass('dis_block');
        $('.post_one .post_mid').addClass('miniatures_post_one miniatures_style');
        $('.post_two .post_mid').addClass('miniatures_post_two miniatures_style');
        $('.post_three.white_bac').removeClass('dis_none_text');
        $('.post_three.white_bac').removeClass('dis_none_text');
        $('.post_icon, .post_top_text, .point_post, .post_class, .post_time').addClass('minpost');
        $('.post_middle').addClass('border_none');
        $('.post_middle_img').removeClass('dis_none');
    });
    $('.view_text').click(function () {
        $('.gray_bar_right .text_tabs, .view_text .tick_type_png').removeClass('dis_none');
        $('.gray_bar_right .two_rectangles, .view_classic .tick_type_png').removeClass('dis_none');
        $('.gray_bar_right .two_rectangles, .view_classic .tick_type_png').addClass('dis_none_text');
        $('.gray_bar_right .text_tabs, .view_text .tick_type_png').addClass('dis_block');
        $('.gray_bar_right .miniatures_tabs, .view_miniatures .tick_type_png').removeClass('dis_block');
        $('.post_one .post_mid').removeClass('miniatures_post_one miniatures_style');
        $('.post_two .post_mid').removeClass('miniatures_post_two miniatures_style');
        $('.post_three.white_bac').removeClass('dis_none');
        $('.post_three.white_bac').addClass('dis_none_text');
        $('.post_icon, .post_top_text, .point_post, .post_class, .post_time').addClass('minpost');
        $('.post_middle').removeClass('border_none');
        $('.post_middle_img').addClass('dis_none');
        $('.post_middle').addClass('border_none');
    });




    $('.click_opacity').click(function () {
        $('.click_opacity').removeClass('active');
        $(this).addClass('active');
    });




    $('.slider_bottom_img').click(function () {
        $(this).toggleClass('active');
        $('.selection_pictures_img').toggleClass('active');
        $('.selection_pictures.mt16px').toggleClass('active');
        $('#thumbs .selection_slider_number').toggleClass('active');
        $('.checkbox-custom').toggleClass('active');
    });

    //
    // $('.profile_black').click(function () {
    //
    //     app.render('view_userdata')
    //     // $('body').toggleClass('hidden_body');
    // });

    // $('.news_feed_img').click(function () {
    //     $('.message_dialog').toggleClass('active');
    //     $('body').toggleClass('hidden_body');
    // });
    $('.arrow_exit').click(function () {
        $('.modal_create').removeClass('active');
        $('body').removeClass('hidden_body');
    });

    $('.selection_pictures.mt16px').click(function () {
        $('.general_img').toggleClass('active');
    });

    $('.cross_exit').click(function () {
        $('.create_photo_modal').removeClass('active');
    });
    // $('.create_post_replacement').click(function () {
    //     $(this).toggleClass('active');
    //     $('.create_photo_modal').toggleClass('active');
    //     $('.create_post_added').toggleClass('active');
    // });


    $('.written_comment').click(function () {
        $('.written_comment_p').toggleClass('active');
    });
    $('.plus_img').click(function () {
        $(this).toggleClass('active');
        $('.minus_img').removeClass('active');
    });
    $('.minus_img').click(function () {
        $(this).toggleClass('active');
        $('.plus_img').removeClass('active');
    });
    $('.answer_open_close').click(function () {
        $('.disp_non').toggleClass('active');
        $('.disp_bloc').toggleClass('active');
        $('.comment_block.anti_row').toggleClass('active');
    });


    $('.entry_search_perent').click(function () {
        $('.search_group_perent, .tabs_search, .initial_search').toggleClass('active');
    });

    $('.perent_without_registration').click(function () {
        $('.start_screen_modal').toggleClass('active');
    });
    $('.perent_join_link').click(function () {
        $('.start_screen_modal').toggleClass('active');
        $('.log_in_madal').toggleClass('active');
    });
    $('.log_in_madal .arrow_exit').click(function () {
        $('.log_in_madal').removeClass('active');
        $('.start_screen_modal').removeClass('active');

        $('.sign_up_madal').removeClass('active');
    });
    $('.perent_enter_weble').click(function () {
        $('.log_in_madal').removeClass('active');
    });
    $('.create_account').click(function () {
        $('.log_in_madal').removeClass('active');
        $('.sign_up_madal').toggleClass('active');
    });

    $('.sign_up_madal .arrow_exit').click(function () {
        $('.sign_up_madal').removeClass('active');
        $('.log_in_madal').toggleClass('active');
    });
    $('.create_account.width_sign').click(function () {
        $('.sign_up_madal').removeClass('active');
        $('.log_in_madal').toggleClass('active');
    });
    $('.perent_registration_weble').click(function () {
        $('.sign_up_madal').removeClass('active');
    });
    $('.forgot_password').click(function () {
        $('.sign_up_madal').removeClass('active');
        $('.forgot_password_madal').toggleClass('active');
    });

    $('.forgot_password_madal .arrow_exit').click(function () {
        $('.forgot_password_madal').removeClass('active');
        $('.log_in_madal').toggleClass('active');
    });

    $('.perent_send_weble').click(function () {
        $('.forgot_password_madal').removeClass('active');
        $('.link_send_madal').toggleClass('active');
    });

    $('.link_send_madal, .link_send_madal .arrow_exit').click(function () {
        $('.link_send_madal').removeClass('active');
        $('.log_in_madal').toggleClass('active');
    });
    $('.sending_messages').click(function () {
        $(this).toggleClass('active');
        $('.writing_message, .messages_footer .arrow_exit, .messages_footer .found_option_link, .click_messages, .messages_footer .arrow_exit_text, .new_messages').toggleClass('active');
    });

    $('.messages_footer .arrow_exit').click(function () {
        $(this).removeClass('active');
        $('.writing_message, .sending_messages, .messages_footer .found_option_link, .click_messages, .messages_footer .arrow_exit_text, .new_messages').removeClass('active');
    });

    $('.textarea_messages').click(function () {
        $('.send_message').toggleClass('active');
    });


    $('.settings_pers').click(function () {
        $('.settings_modal').addClass('active');
    });
    $('.settings_modal .arrow_exit').click(function () {
        $('.settings_modal').removeClass('active');
    });


    $('.my_subscr').click(function () {
        $('.my_subscribers_modal').addClass('active');
    });
    $('.my_subscribers_modal .arrow_exit').click(function () {
        $('.my_subscribers_modal').removeClass('active');
    });

    $('.settings_notification').click(function () {
        $('.settings_push_modal').addClass('active');
        $('.settings_modal').removeClass('active');
    });
    $('.settings_push_modal .arrow_exit').click(function () {
        $('.settings_push_modal').removeClass('active');
        $('.settings_modal').addClass('active');
    });



    $('.settings_personal').click(function () {
        $('.user_data_modal').addClass('active');
        $('.settings_modal').removeClass('active');
    });
    $('.user_data_modal .arrow_exit').click(function () {
        $('.user_data_modal').removeClass('active');
        $('.settings_modal').addClass('active');
    });



    $('.pers_name_text, .settings_accounts').click(function () {
        $('.acc_select_modal').addClass('active');
    });


    $('.acc_select_opacity').click(function () {
        $('.acc_select_modal').removeClass('active');
    });

    $('.none_subscribed').click(function () {
        $('.subscribe_modal').addClass('active');
    });
    $('.subscribe_tab').click(function () {
        $('.subscription_unsubscribe').addClass('news_subscribed');
        $('.subscription_unsubscribe').removeClass('none_subscribed');
        $('.subscribe_modal').removeClass('active');
    });

    $('.unsubscribe_undo').click(function () {
        $('.subscribe_modal, .unsubscribe_modal').removeClass('active');
    });

    $('.news_subscribed').click(function () {
        $('.unsubscribe_modal').addClass('active');
    });
    $('.unsubscribe_tab').click(function () {
        $('.subscription_unsubscribe').removeClass('news_subscribed');
        $('.subscription_unsubscribe').addClass('none_subscribed');
        $('.unsubscribe_modal').removeClass('active');
    });



    $('.my_subscrip').click(function () {
        $('.my_subscriptions_modal').addClass('active');
    });
    $('.my_subscriptions_modal .arrow_exit').click(function () {
        $('.my_subscriptions_modal').removeClass('active');
    });


    $('.checkbox_messages, .write_message').click(function () {
        $('.message_dialog').toggleClass('active');
    });

    $('.message_dialog .arrow_exit').click(function () {
        $('.message_dialog').removeClass('active');
        $(".dialog_block").scrollTop($('.response_interlocutor').offset().top);
        $('.send_message').removeClass('active');
    });

    $('.message_dialog .textarea_messages').click(function () {
        $('.send_message, .dialog_block, .keyboard, .message_dialog .send_message').addClass('active');
    });


    $('.main_slider').owlCarousel({
        items:1,
        loop:true,
        margin:0,
        autoWidth: false,
    });

    $('.main_slider, .main_search_slider').bind(
        'touchmove',
        function(e) {
            e.preventDefault();
        }
    );


    var largeImg = document.getElementById('largeImg');

    var thumbs = document.getElementById('thumbs');

    // thumbs.onclick = function (e) {
    //     var target = e.target;
    //
    //     while (target != this) {
    //
    //         if (target.nodeName == 'A') {
    //             showThumbnail(target.href, target.title);
    //             return false;
    //         }
    //
    //         target = target.parentNode;
    //     }
    //
    // }

    function showThumbnail(href, title) {
        largeImg.src = href;
        largeImg.alt = title;
    }


    /* РїСЂРµРґР·Р°РіСЂСѓР·РєР° */
    // var imgs = thumbs.getElementsByTagName('img');
    // for (var i = 0; i < imgs.length; i++) {
    //     var url = imgs[i].parentNode.href;
    //     var img = document.createElement('img');
    //     img.src = url;
    // }


    $('.search_slider').owlCarousel({
        loop: true,
        margin: 8,
        nav: true,
        autoWidth: true,
        items: 4,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

})
