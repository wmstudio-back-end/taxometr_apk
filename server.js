var WebSocketServer = require('ws').Server
  , http = require('http')
  , express = require('express')
  , app = express()
  , port = process.env.PORT || 7001;

ObjectID = require('mongodb').ObjectID
ObjectId = require('mongodb').ObjectID
WebSocket = require('ws');
global.db = {};
USERS = {}
var MongoClient = require('mongodb').MongoClient;
MongoClient.connect('mongodb://taxi:taxi@localhost:27017/taxi', function(err, db) {
  if (err){
    console.log("DB ERROR ",err)
    return;
  }

  // global.dbConnect = db
  db = db.db('taxi')
  global.db.drivers = db.collection('drivers');
  global.db.admins = db.collection('admins');
  global.db.orders = db.collection('orders');
  next()
})

function next(){

  app.use(express.static(__dirname + '/'));

  var server = http.createServer(app);
  server.listen(port);

  console.log('http server listening on %d', port);

  var wss = new WebSocketServer({server: server});

  wss.on('connection', function connection(ws) {
    var cookies

    ws.on('message',  onMessage);
    ws.on('finish',() => {
      ws.destroy();
    });
    ws.on('error', (error) => {
      console.log('error', error);
    });


  });

}
onMessage = function(res){
  var $this = this
  var r = false
  var msg = JSON.parse(res)
  if (msg.hasOwnProperty('method')&&controllers.hasOwnProperty(msg.method)){
    if (!this._id){
      if (msg.method=='authDriver'||msg.method=='authAdmin'){
        r = true
        console.log("MESSAGE",this._id,msg.method)
      }

    }else{r = true}
    if (r){
      controllers[msg.method](msg.data,function(err,data){
        msg.data = data
        msg.error = err

        $this.send(JSON.stringify(msg));
      },$this);
    }

  }else{
    console.log('NO METHOD');
  }

}
var controllers = {
  getHistory: function(msg,callback,ws){
    global.db.orders.find({}).toArray(function(err,data){
      callback(err,data)
    })

  },
  setGPSDriver : function(msg,callback,ws){
    if (USERS[ws._id]){
      USERS[ws._id].profile.gps = msg.gps
      console.log('UPDATE GPS',ws._id)

      callback(null,{result:true})
    }

  },
  authAdmin : function(msg,callback,ws){
    if (msg.hasOwnProperty('code')){
      global.db.admins.findOne({code:msg.code},function(err,data){
        console.log(data)
        if (data){
          ws._id = data._id
          callback(err,data)
        }else{

          callback({err:'notfound'},null)
        }


      })
    }
    if (msg.hasOwnProperty('token')){
      global.db.admins.findOne({_id:ObjectId(msg.token)},function(err,data){
        ws._id = data._id
        callback(err,data)

      })
    }
  },
  authDriver : function(msg,callback, ws){

    if (msg.hasOwnProperty('code')){
      global.db.drivers.findOne({code:msg.code},function(err,data){

        console.log("-------setSe---code-----",data._id)
        USERS[data._id] = {
          profile : data,
          ws : ws
        }
        ws._id = data._id
        callback(err,data)

      })
    }
    if (msg.hasOwnProperty('token')){
      console.log(msg.token)
      global.db.drivers.findOne({_id:ObjectId(msg.token)},function(err,data){
        console.log("-------setSe----token----",data,err)
        USERS[data._id] = {
          profile : data,
          ws : ws
        }
        ws._id = data._id
        callback(err,data)
      })
    }
  },
  calculateOrder:function(msg,callback=function(){}){
    var price = 0


    switch (msg.tariff) {
      case 1:

          if (msg.time<3600){msg.time = 0;}
          else{
            msg.time -=3600
          }
        console.log('меньше часа',msg.time)
          var o = (msg.time/3600)
        console.log('msg.time-3600/3600',o)
          price = typeAvto[msg.type].firstHour
        console.log('price',price)
          price += o*typeAvto[msg.type].nextHour
        break
      case 2:
        price = typeAvto[msg.type].supply
        price += (msg.length/1000)*typeAvto[msg.type].kilometer
        break;
      case 3:
        price = typeAvto[msg.type].supply
        price += (msg.length/1000)*typeAvto[msg.type].kilometerback
        break;
    }
    callback(null,{price:price})
    return price
  },
  createInitOrder:function(msg,callback,ws){
    var price = controllers.calculateOrder(msg)
    msg.status = 0
    msg.priceStart = price
    msg.created_at = new Date()
    msg.admin_id = ObjectId(ws._id)
    global.db.orders.save(msg,function(err,data){
      var res = data['ops'][0]
      console.log(res)
      var uid = findNearUser(res.path[0].coordinates[0],res.path[0].coordinates[1])
      if (uid){
        USERS[uid].ws.send(JSON.stringify({method:'createInitOrder',data:res,error:null,requestId:1}))
      }
      callback(null,{response:res})

    })


  },
  createInitOrder2:function(msg,callback,ws){
    var t = {
      "_id" : ObjectId("5c7cb46bc3ffa602cc9d3161"),
      "length" : 186799.57,
      "time" : 10042.04,
      "tariff" : 2,
      "type" : 2,
      "path" : [
      {
        "address" : "Россия, Удмуртская Республика, Ижевск, улица Василия Чугуевского, 9",
        "coordinates" : [
          53.229717,
          56.8352
        ]
      },
      {
        "address" : "Россия, Кировская область, Кильмезский район",
        "coordinates" : [
          51.0313992816742,
          56.9678571771953
        ]
      }
    ],
      "status" : 0,
      "priceStart" : 6924.78452,
      "created_at" : new Date()
    }
      callback(null,t)

    }

}


findNearUser = function(lat,lon){
  console.log("findNearUser")
  var min = {
    uid:null,
    dist:999999999999
  }
  for (var key in USERS) {
    console.log(USERS[key].profile)
    if (USERS[key].profile&&USERS[key].profile.gps){
      USERS[key].profile.gps.lat
      USERS[key].profile.gps.lon
      var mx=Math.abs(USERS[key].profile.gps.lat-lat);
      var my=Math.abs(USERS[key].profile.gps.lon-lon);
      var dist=Math.sqrt(Math.pow(mx,2)+Math.pow(my,2));
      if (min.dist>dist){
        min.uid = key
        min.dist = dist
      }
      if (min.uid){
        return min.uid

        // console.log('SEND!!!!!!!!!!!!!!!!!!!!!!',USERS[min.uid].ws)
      }

      console.log("DIST!!!!",dist)
    }

  }
  return null
  console.log('RES', min)

}
 var typeAvto = {
  "1":{
    name:"каблук",
    maxWeight:600,
    maxLength:200,
    firstHour:500,
    nextHour:400,
    kilometer:20,
    kilometerback:20,
    supply:200

  },
   "2":{
     name:"Газель 3м",
     maxWeight:1500,
     maxLength:3000,
     firstHour:700,
     nextHour:600,
     kilometer:36,
     kilometerback:18,
     supply:200
   },
   "3":{
     name:"Газель 4м",
     maxWeight:1500,
     maxLength:4000,
     firstHour:800,
     nextHour:600,
     kilometer:36,
     kilometerback:18,
     supply:200
   },
   "4":{
     name:"Газель 6м",
     maxWeight:1500,
     maxLength:6000,
     firstHour:900,
     nextHour:600,
     kilometer:36,
     kilometerback:18,
     supply:200
   },
   "5":{
     name:"Газ 5 тонн",
     maxWeight:3000,
     maxLength:5000,
     firstHour:1100,
     nextHour:800,
     kilometer:40,
     kilometerback:20,
     supply:200
   }
 }
