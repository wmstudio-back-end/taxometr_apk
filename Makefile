#
# Tasks
#
# - start   :: starts application using forever
# - stop    :: stops application using forever
# - update :: update application and build client
#

# This set's your local directory to to your NODE_PATH
NODE_EXEC    = NODE_PATH=.:$(NODE_PATH)

# This is for local (non `-g`) npm installs.
#NODE_MODS    = ./node_modules/.bin/

# Some good `talents` options.
FOREVER_OPTS = -p ./logs  \
		-o ./logs/server_out.log \
		-e ./logs/server_err.log \
		--append \
		--plain \
		--minUptime 1000 \
		--spinSleepTime 1000

local = -p ./logs  \
		-l server_out.log \
		-o ./logs/server_out.log \
		-e ./logs/server_err.log \
		--append \
		--plain \
		--serv_client_port 80 \
		--serv_worker_port 50052 \
		--serv_worker_host localhost \
		--serv_static_httpport 8182 \
		--serv_static_host localhost \
		--serv_graphql_port 30052 \
		--serv_graphql_host localhost \
		--serv_wsproxy_wsport 7000 \
		--serv_wsproxy_wshost localhost \
		--serv_wsproxy_httpport 8888

prod = -p ./logs  \
		-l server_out.log \
		-o ./logs/server_out.log \
		-e ./logs/server_err.log \
		--append \
		--plain \
		--https \
		--https \
		--serv_client_port 8080 \
		--serv_worker_port 50052 \
		--serv_worker_host localhost \
		--serv_static_httpport 8182 \
		--serv_static_host localhost \
		--serv_graphql_port 30052 \
		--serv_graphql_host localhost \
		--serv_wsproxy_wsport 7000 \
		--serv_wsproxy_wshost talents.fund \
		--serv_wsproxy_httpport 8888

evgen = -p ./logs  \
		-l server_out.log \
		-o ./logs/server_out.log \
		-e ./logs/server_err.log \
		--append \
		--plain \
		--prodaction 0 \
		--serv_client_port 80 \
		--serv_worker_port 50052 \
		--serv_worker_host 192.168.102.231 \
		--serv_static_httpport 8182 \
		--serv_static_host 192.168.102.231 \
		--serv_graphql_port 30052 \
		--serv_graphql_host 192.168.102.231 \
		--serv_wsproxy_wsport 7000 \
		--serv_wsproxy_wshost 192.168.102.231 \
		--serv_wsproxy_httpport 8888 \
		--serv_cms_host wms-dev.xyz \
		--serv_cms_port 443

start: setup/dirs
  # starting apps in server mode
	#cd ./aggr_notification/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ index.js  --config=$($(c))
#	cd ./aggr_match/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ index.js  --config=$($(c))
	#cd ./aggr_services/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ index.js  --config=$($(c))
	#cd ./worker_vacancy/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ index.js  --config=$($(c))
	cd ./serv_worker/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ gfserver.js  --config=$($(c))
	cd ./serv_static/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ gfserver.js  --config=$($(c))
	cd ./serv_wsproxy/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ gfserver.js --config=$($(c))
	#cd ./client/ && mkdir -p logs pids && $(NODE_EXEC) $(NODE_MODS)sudo forever $(FOREVER_OPTS) $@ run.js  --config=$($(c))


update: setup/dirs
	# updating apps in server mode and build client
	cd ./serv_static && git pull origin master && npm i
	cd ./serv_worker && git pull origin master && npm i
	cd ./serv_wsproxy && git pull origin master && npm i
	#cd ./client && git pull origin master && npm i && npm run build

build:
	# build client angular
	cd ./client && npm run build

stop:
	# stopping all apps
	sudo forever stopall

backup: setup/dirs
	# testing   to do
	#$(NODE_EXEC) $(NODE_MODS)forever $(FOREVER_OPTS) $@ ./serv_static/server.js
  #mongodump -h localhost -d project -o ./backup/project
  #mongorestore -h localhost -d project ./backup/project

restart: setup/dirs
	# restarting app in server mode
	#$(NODE_EXEC) $(NODE_MODS)forever $(FOREVER_OPTS) $@ ./serv_static/server.js

setup/dirs:
	# creating required directories for `forever`
	mkdir -p logs pids


